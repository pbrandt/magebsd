#!/usr/bin/env sh

# THIS IS NOT BASH, THIS IS SH

# set up local variables for this script
MAGEBSD="$(dirname "$(readlink -f "$0")")"
USRHOME="$(cd ~ && pwd)"

echo MageBSD is at $MAGEBSD
echo User home is $USRHOME

set -e

# success and fail helper functions
success() {
  echo -e "\e[32m [ok]\e[0m $@"
}

fail() {
  echo -e "\e[31m [bad]\e'[0m $@"
  exit 1
}

# user should run this in userland, not as root
if [ $(id -u) = 0 ] ; then
  fail "cannot run this script as root"
fi

# however the user does need sudo access. update that here if the files are different
if diff /usr/local/etc/sudoers "$MAGEBSD/sudoers" >/dev/null ; then
  success "sudoers file up to date"
else
  if visudo -c -f "$MAGEBSD/sudoers" >/dev/null ; then
    echo "Please enter root password to update the sudoers file"
    su -m root -c "cat $MAGEBSD/sudoers > /usr/local/etc/sudoers"
    success "sudoers file updated"
  else
    fail "sudoers file failed syntax check. edit it and run visudo -c -f [file] to manually check before re-running this script"
  fi
fi

# install packages
echo installing packages
while read LINE ; do
  # delete everything after #
  # trim whitespace
  # if anything is left, install it

  LINE="$(echo "$LINE" | cut -f1 -d"#")" # removes ever
  LINE="$(echo "$LINE" | xargs)" # trims whitespace

  if [ "$LINE" = "" ] ; then
    # it's a comment
  else
    if ! pkg info "$LINE" >/dev/null ; then
      sudo pkg install -yq "$LINE" >/dev/null
    fi
    success $LINE installed
  fi
done < "$MAGEBSD/packages"


# link files to correct locations
ln -sf "$MAGEBSD/bashrc" "$USRHOME/.bashrc"
ln -sf "$MAGEBSD/vimrc" "$USRHOME/.vimrc"
ln -sf "$MAGEBSD/gitconfig" "$USRHOME/.gitconfig"
ln -sf "$MAGEBSD/Xdefaults" "$USRHOME/.Xdefaults"
ln -sf "$MAGEBSD/xinitrc" "$USRHOME/.xinitrc"
ln -sf "$MAGEBSD/bash_profile" "$USRHOME/.bash_profile"

# update default shell
echo checking shell
if [ "$SHELL" = "/usr/local/bin/bash" ]; then
  success user default shell is $SHELL
else

fi


