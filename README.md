# MageBSD

Bootstrap your FreeBSD/TrueOS intall.

Features:
- i3
- text file configuration
- forkable/customizable
- strives to do everything The Right Way (TM)

Usage:

```sh
pkg install -y git
git clone [this repo]
cd MageBSD
./update.sh
```

Then every time you make a configuration change or you want to pull latest dependencies, run `update.sh` again.


