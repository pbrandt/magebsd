

there are lots of fonts you can install

which one to use

http://futurile.net/2016/06/14/xterm-setup-and-truetype-font-configuration/

list fonts with fc-list

```sh
fc-list :scalable=true:spacing=mono: family
```

changing font size

```
xterm*VT100.Translations: #override \
    Ctrl <Key> minus: smaller-vt-font() \n\
    Ctrl <Key> plus: larger-vt-font() \n\
    Ctrl <Key> 0: set-vt-font(d)
```

more xterm configuration

```
xterm*VT100.Translations: #override \
    Shift <KeyPress> Insert: insert-selection(CLIPBOARD) \n\
    Ctrl Shift <Key>V:    insert-selection(CLIPBOARD) \n\
    Ctrl Shift <Key>C:    copy-selection(CLIPBOARD) \n\
    Ctrl <Btn1Up>: exec-formatted("xdg-open '%t'", PRIMARY)

```


WHEN IN DOUBT: run fcitx-diagnose

with that i found that my `LC_CTYPE` env variable was set to POSIX not en_US.UTF-8. I changed it and now i can use fcitx with xterm


