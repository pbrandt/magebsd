# MageBSD docs

Table Of Contents

- [hardware.md](hardware.md) - how hardware is configured, like screen brightness keys
- [networking.md](networking.md) - how networking is configured, like mdnsd

