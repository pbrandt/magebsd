
## default shell

Leave root shell as sh. Change user default shell to bash/zsh/fish.


## sudo

All users in the `wheel` group will be given sudo access. The name "wheel" was derived from the slang phrase "big wheel", referring to a person with great power of influence [(wikipedia)](https://en.wikipedia.org/wiki/Wheel_(computing)). Ubuntu uses the "sudo" group, which is arguably easier to understand, but this is bsd and so we'll use wheel and at least we know the back story now.

The sudoers file is located at `/usr/local/etc/sudoers` and you can use `visudo -c -f [file]` to check the syntax of your own file before copying it in place there.

In the `update.sh` script, the sudoers file is updated in the following way:

```sh
su -m root -c "cat $MAGEBSD/sudoers > /usr/local/etc/sudoers"
```

The `-m` means that the environment will be left unmodified, and `root` means to switch to the root user. For the ">" part in the command, I needed to figure out how to overwrite files without changing the uid and gid of the target file, and the output redirect appears to do what i want.


## xterm

we're using uxterm, for utf-8 in xterm

ctrl+left mouse button for main options

ctrl+right mouse button for fonts menu



Colors for `ls`

options: use freebsd ls or use gnu ls

gnu ls lets you use fancy colors depending on the filename, like colorizing all png files etc. freebsd does not, but it lets you colorize things on the type of file they appear to be to the system: dir, link, socket, executable, etc.\

freebsd ls

from https://www.cyberciti.biz/tips/freebsd-how-to-enable-colorized-ls-output.html

```
The color designators are as follows:

      a     black
      b     red
      c     green
      d     brown
      e     blue
      f     magenta
      g     cyan
      h     light grey
      A     bold black, usually shows up as dark grey
      B     bold red
      C     bold green
      D     bold brown, usually shows up as yellow
      E     bold blue
      F     bold magenta
      G     bold cyan
      H     bold light grey; looks like bright white
      x     default foreground or background

Note that the above are standard ANSI colors.  The actual
display may differ depending on the color capabilities of
the terminal in use.

The order of the attributes are as follows:

      1.   directory
      2.   symbolic link
      3.   socket
      4.   pipe
      5.   executable
      6.   block special
      7.   character special
      8.   executable with setuid bit set
      9.   executable with setgid bit set
      10.  directory writable to others, with sticky bit
      11.  directory writable to others, without sticky bit

The default is "exfxcxdxbxegedabagacad", i.e., blue fore-
ground and default background for regular directories,
black foreground and red background for setuid executa-
bles, etc.
```


gnu ls

```
pkg install gnuls
alias ls=gnuls
```


