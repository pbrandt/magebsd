# normal tasks
- [ ] figure out text file configuration format
- [x] pkg installer
- [ ] update and audit packages
- [x] xterm colors
- [ ] ls colors and bolding of directories
- [x] chinese input
- [x] add user to sudoers
- [ ] add user groups
- [ ] generate ssh keys
- [ ] sane copy/paste behavior (https://askubuntu.com/questions/237942/how-does-copy-paste-work-with-xterm)
- [ ] fonts (https://vermaden.wordpress.com/2018/08/18/freebsd-desktop-part-15-configuration-fonts-frameworks/)
- [ ] vim with xterm play nicely, don't leave the previous screen in terminal when quit vim
- [ ] xterm scroll wheel
- [ ] volume keys
- [ ] screen brightness keys
- [x] set default shell
- [ ] make sure the bash setup is correct
- [ ] add fancy bash aliases
- [ ] make sure audio works
- [ ] make sure google hangouts works
- [ ] notifications. see "alert" in bashrc, uses notify-send. also my hq stuff.
- [ ] put something random in /etc/machine-id for dbus/dconf/whatever uses it
- [ ] ssh keygen
- [ ] configure i3
- [ ] make an i3 autostart that opens up my setup
- [ ] ~/.config/fcitx
- [ ] terminal that works with fcitx
- [ ] korean input


# software to figure out defaults for
- [ ] pdf viewer
- [ ] web browser
- [ ] office suite
- [ ] file browser
- [ ] screenshot
- [ ] screen capture to gif


# things to think about
- [ ] zfs and rollbacks
- [ ] make tutorial for zfs and rollbacks
- [ ] 




# bigger things - Mage Apps

- [ ] make a simple mdnsd for freebsd that is literally zero conf aka no options, spin and win
- [ ] info screen command line tool: battery, mem, disk, cpu, internet speed, volume, time, language
- [ ] rpg-style file browser where your avatar walks around a jrpg like visual representation of your files and applications. like a memory palace for your code

